export const blue = {

  '--primary' : '#063a6f',
  '--primary-variant': '#eef6e3',

  '--secondary': '#2a3a42',
  '--secondary-variant': '#eef6e3',

  '--background': '#8EE4AF',
  '--surface': '#eef6e3',
  '--dialog': '#eef6e3',
  '--cancel': '#336e74',
  '--alt-surface': '#dee5d4',
  '--alt-dialog': '#dee5d4',

  '--on-primary': '#ffffff',
  '--on-secondary': '#ffffff',
  '--on-background': '#000000',
  '--on-surface': '#000000',
  '--on-cancel': '#ffffff',

  '--green': 'green',
  '--red': 'red',
  '--yellow': '#fa4d0d',
  '--blue': 'blue',
  '--purple': 'purple',
  '--light-green': 'lightgreen',
  '--grey': 'grey',
  '--grey-light': 'lightgrey',
  '--black': 'black',
  '--moderator': '#fff3e0'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Mint Theme',
      'de': 'Mint Theme'
    },
    'description': {
      'en': 'Contrast compliant with WCAG 2.1 AA',
      'de': 'Helligkeitskontrast nach WCAG 2.1 AA'
    }
  },
  'isDark': false,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};

